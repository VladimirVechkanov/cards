﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Cards {
	public class Card : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IDragHandler, IEndDragHandler {
		[SerializeField]
		private GameObject _frontCard;
		[SerializeField]
		private MeshRenderer _picture;
		[SerializeField]
		private TextMeshPro _name;
		[SerializeField]
		private TextMeshPro _description;
		[SerializeField]
		private TextMeshPro _cost;
		[SerializeField]
		private TextMeshPro _attack;
		[SerializeField]
		private TextMeshPro _health;
		[SerializeField]
		private TextMeshPro _type;

		public CardsStateType StateType { get; set; } = CardsStateType.InDeck;
		public PlayerNumber PlayerNumber { get; set; }
		public bool IsFrontSide => _frontCard.activeSelf;

		public Vector3 offset;

		public void Configuration(CardPropertiesData data, Material picture, string description) {
			_name.text = data.Name;

			var array = description.Replace("<b>", "").Replace("</b>", "").Replace("\n", "").Trim().Split(' ');
			if(array.Length <= 4) {
				_description.fontSize = 1.5f;
			}
			_description.text = description;
			_cost.text = data.Cost.ToString();
			_attack.text = data.Attack.ToString();
			_health.text = data.Health.ToString();
			_type.text = data.Type == CardUnitType.None ? "" : data.Type.ToString();

			_picture.material = picture;
		}

		public void OnBeginDrag(PointerEventData eventData) {
			if(StateType == CardsStateType.InHand || StateType == CardsStateType.OnTable) {
				var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				if(Physics.Raycast(ray, out var hit)) {
					offset = transform.position - Camera.main.transform.position;
				}
			}
		}

		public void OnDrag(PointerEventData eventData) {
			if(StateType == CardsStateType.InHand || StateType == CardsStateType.OnTable) {
				var pos = Input.mousePosition + offset;
				pos = Camera.main.ScreenToWorldPoint(pos);
				transform.position = new Vector3(pos.x, transform.position.y, pos.z);
			}
		}

		public void OnEndDrag(PointerEventData eventData) {
			if(StateType == CardsStateType.InHand || StateType == CardsStateType.OnTable) {
				
			}
		}

		public void OnPointerEnter(PointerEventData eventData) {
			var offsetPos = 30f;
			var offsetScl = 35f;
			if(StateType == CardsStateType.InHand) {
				transform.position += new Vector3(0f, offsetPos, PlayerNumber == PlayerNumber.Player1 ? offsetPos : -offsetPos) ;
				transform.localScale += new Vector3(offsetScl, 0f, offsetScl);
			}
		}

		public void OnPointerExit(PointerEventData eventData) {
			var offsetPos = 30f;
			var offsetScl = 35f;
			if(StateType == CardsStateType.InHand) {
				transform.position -= new Vector3(0f, offsetPos, PlayerNumber == PlayerNumber.Player1 ? offsetPos : -offsetPos);
				transform.localScale -= new Vector3(offsetScl, 0f, offsetScl);
			}
		}

		[ContextMenu("Switch Enable")]
		public void SwitchEnable() {
			var boolean = !IsFrontSide;
			_frontCard.SetActive(boolean);
			_picture.enabled = boolean;
		}
	}
}