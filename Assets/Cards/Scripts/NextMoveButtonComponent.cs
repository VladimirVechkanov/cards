﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Cards {
	public class NextMoveButtonComponent : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler {
		[SerializeField]
		private GameManager _gameManager;
		[SerializeField]
		private Material _material;
		private Color _color;

		private void Start() {
			_color = _material.color;
		}

		public void OnPointerDown(PointerEventData eventData) {
			transform.localScale -= new Vector3(0f, transform.localScale.y / 2, 0f);
		}

		public void OnPointerUp(PointerEventData eventData) {
			transform.localScale += new Vector3(0f, transform.localScale.y, 0f);
			_gameManager.NextMove();
		}

		public void OnPointerEnter(PointerEventData eventData) {
			_material.color = Color.yellow;
		}

		public void OnPointerExit(PointerEventData eventData) {
			_material.color = _color;
		}
	}
}