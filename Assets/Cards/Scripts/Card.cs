﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Cards {
	public class Card : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IDragHandler, IEndDragHandler {
		[SerializeField]
		private GameObject _frontCard;
		[SerializeField]
		private MeshRenderer _picture;
		[SerializeField]
		private TextMeshPro _name;
		[SerializeField]
		private TextMeshPro _description;
		[SerializeField]
		private TextMeshPro _cost;
		[SerializeField]
		private TextMeshPro _attack;
		[SerializeField]
		private TextMeshPro _health;
		[SerializeField]
		private TextMeshPro _type;

		public CardsStateType StateType { get; set; } = CardsStateType.InDeck;
		public CardUnitType CardUnitType { get; set; }
		public PlayerNumber PlayerNumber { get; set; }
		public PlayerTable PlayerTable { get; set; }
		public bool IsFrontSide => _frontCard.activeSelf;

		private GameManager _gameManager;

		private Vector3 _lastPosition;
		private Vector3 _normalScale = new Vector3(70f, 1f, 100f);
		private bool _onPointerStay;

		[NonSerialized]
		public int Cost;
		[NonSerialized]
		public int Attack;
		[NonSerialized]
		public int Health;
		[NonSerialized]
		public int Id;

		private bool _taunt;
		private bool _divineShield;
		private bool _battlecry;
		private bool _deathrattle;
		private bool _lifesteal;

		private bool _canMove = true;

		private void Start() {
			_gameManager = FindObjectOfType<GameManager>();
		}

		public void Configuration(CardPropertiesData data, Material picture, string description) {
			_name.text = data.Name;

			var array = description.Replace("<b>", "").Replace("</b>", "").Replace("\n", "").Trim().Split(' ');
			if(array.Length <= 4) {
				_description.fontSize = 1.5f;
			}
			_description.text = description;
			_cost.text = data.Cost.ToString();
			_attack.text = data.Attack.ToString();
			_health.text = data.Health.ToString();
			_type.text = data.Type == CardUnitType.None ? "" : data.Type.ToString();
			
			_picture.material = picture;

			Cost = data.Cost;
			Attack = data.Attack;
			Health = data.Health;

			if(description.ToLower().Contains("taunt")) {
				_taunt = true;
			}
			if(description.ToLower().Contains("divine shield")) {
				_divineShield = true;
			}
			if(description.ToLower().Contains("rush")) {
				_canMove = true;
			}
			if(description.ToLower().Contains("battlecry")) {
				_battlecry = true;
			}
			if(description.ToLower().Contains("deathrattle")) {
				_deathrattle = true;
			}
			if(description.ToLower().Contains("lifesteal")) {
				_lifesteal = true;
			}
		}

		public void OnBeginDrag(PointerEventData eventData) {
			_lastPosition = transform.position;
		}

		public void OnDrag(PointerEventData eventData) {
			if(_gameManager._playerMove != PlayerNumber) return;

			if(StateType == CardsStateType.InHand || StateType == CardsStateType.OnTable) {
				var pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y));
				transform.position = new Vector3(pos.x, transform.position.y, pos.z);
			}
		}

		public void OnEndDrag(PointerEventData eventData) {
			if(_gameManager._playerMove != PlayerNumber || !_canMove) return;

			if(StateType == CardsStateType.InHand || StateType == CardsStateType.OnTable) {
				var hits = Physics.RaycastAll(transform.position, transform.up, 10000f);
				if(Physics.Raycast(transform.position, transform.up, out var hit, 10000f)) {
					var table = hit.transform.GetComponent<PlayerTable>();
					var card = hit.transform.GetComponent<Card>();
					if(card == null) {
						card = hit.transform.GetComponentInParent<Card>();
					}
					var face = hit.transform.GetComponent<PlayerFace>();
					var placeInHand = hit.transform.GetComponent<PlaceInHand>();
					var placeInTable = hit.transform.GetComponent<PlaceInTable>();
					var tag = hit.transform.tag;

					if(hit.transform.name == "Table") {
						StartCoroutine(MoveInLastPosition());
					}

					if(StateType == CardsStateType.InHand) {
						if(placeInTable != null || table != null || card != null && card.StateType == CardsStateType.OnTable) {
							if(_gameManager.CanMoveCardToTable(this)) {
								if(!_gameManager.MoveCardInTable(this)) {
									StartCoroutine(MoveInLastPosition());
								}
							}
							else {
								StartCoroutine(MoveInLastPosition());
							}
						}
						else {
							StartCoroutine(MoveInLastPosition());
						}
					}
					else if(StateType == CardsStateType.OnTable) {
						if(placeInTable != null || table != null) {
							StartCoroutine(MoveInLastPosition());
						}
						else if(card != null && card.PlayerNumber != PlayerNumber && card.StateType == CardsStateType.OnTable) {
							StartCoroutine(DoAttack(card));
						}
						else if(face != null && face.PlayerNumber != PlayerNumber) {
							face.TakeDamage(Attack);
							StartCoroutine(MoveInLastPosition());
						}
						else {
							StartCoroutine(MoveInLastPosition());
						}
					}
					else {
						StartCoroutine(MoveInLastPosition());
					}
				}
				else {
					StartCoroutine(MoveInLastPosition());
				}
			}
		}

		private IEnumerator DoAttack(Card target) {
			var time = 0f;
			var startPos = transform.position;
			var endPos = target.transform.position;

			while(transform.position != endPos) {
				transform.position = Vector3.Lerp(startPos, endPos, time * 3f);
				time += Time.deltaTime;
				yield return null;
			}

			StartCoroutine(DoTremor());
			target.StartCoroutine(target.DoTremor());

			time = 0f;
			while(transform.position != startPos) {
				transform.position = Vector3.Lerp(endPos, startPos, time * 3f);
				time += Time.deltaTime;
				yield return null;
			}

			target.TakeDamage(Attack);
			TakeDamage(target.Attack);

			StartCoroutine(MoveInLastPosition());
		}

		public IEnumerator DoTremor() {
			var time = 0f;
			var startRot = transform.rotation;

			while(time < 0.5f) {
				transform.rotation = Quaternion.Euler(startRot.eulerAngles.x, UnityEngine.Random.Range(-2f, 2f), startRot.eulerAngles.z);
				time += Time.deltaTime;
				yield return null;
			}

			transform.rotation = startRot;
		}

		public void OnPointerEnter(PointerEventData eventData) {
			if(_gameManager._playerMove != PlayerNumber) return;

			var offsetPos = 30f;
			var offsetScl = 35f;
			if(StateType == CardsStateType.InHand || StateType == CardsStateType.OnTable) {
				transform.position += new Vector3(0f, offsetPos, 0f) ;
				transform.localScale += new Vector3(offsetScl, 0f, offsetScl);

				_onPointerStay = true;
			}
		}

		public void OnPointerExit(PointerEventData eventData) {
			if(_gameManager._playerMove != PlayerNumber) return;

			var offsetPos = 30f;
			var offsetScl = 35f;
			if(StateType == CardsStateType.InHand || StateType == CardsStateType.OnTable) {
				transform.position -= new Vector3(0f, transform.position.y < 30f ? 0f : offsetPos, 0f);
				transform.localScale -= new Vector3(offsetScl, 0f, offsetScl);

				_onPointerStay = false;
			}

			if(transform.localScale != _normalScale) {
				ReturnTONormalScale();
			}
		}

		[ContextMenu("Switch Enable")]
		public void SwitchEnable() {
			var boolean = !IsFrontSide;
			_frontCard.SetActive(boolean);
			_picture.enabled = boolean;
		}

		private IEnumerator MoveInLastPosition() {
			var time = 0f;
			var startPos = transform.position;
			var endPos = new Vector3(_lastPosition.x, _lastPosition.y - (transform.position.y < 30f ? 0f : 30f), _lastPosition.z);

			while(transform.position != endPos) {
				transform.position = Vector3.Lerp(startPos, endPos, time * 4f);
				time += Time.deltaTime;
				yield return null;
			}

			if(!_onPointerStay && transform.position.y != 0.9f) {
				ReturnTONormalY();
			}
		}

		public void TakeDamage(int damage) {
			Health -= damage;
			_health.text = Health.ToString();

			if(Health <= 0) {
				StateType = CardsStateType.Discard;
				this.PlayerTable.RemoveDestroyCards();
				Destroy(gameObject);
			}
		}

		public void ReturnTONormalScale() {
			transform.localScale = _normalScale;
		}

		public void ReturnTONormalY() {
			transform.position = new Vector3(transform.position.x, 0.9f, transform.position.z);
		}
	}
}