﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards
{
	public enum CardUnitType : byte
	{
		None = 0,
		Murloc = 1,
		Beast = 2,
		Elemental = 3,
		Mech = 4,
		Demon = 5,
		Pirate = 6,
		Dragon = 7
	}

	public enum SideType : byte
	{
		Common = 0,
		Mage = 1,
		Warrior = 2,
		Priest = 3,
		Hunt = 4,
		Druid = 5,
		Paladin = 6,
		Rogue = 7,
		Shaman = 8
	}

	public enum CardsStateType : byte
	{
		InDeck,
		InHand,
		OnTable,
		Discard
	}

	public enum PlayerNumber : byte
	{
		Player1,
		Player2
	}
}
