﻿using Cards.ScriptableObjects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

namespace Cards {
	public class GameManager : MonoBehaviour {
		[SerializeField]
		private CardPackConfiguration[] _packs;
		[SerializeField]
		private Card _cardPrefab;

		[SerializeField, Space, Range(5f, 50f)]
		private int _cardDeckCount = 30;

		private Shader _baseMat;
		private List<CardPropertiesData> _allCards;

		[SerializeField, Space]
		private PlayerHand _player1Hand;
		[SerializeField]
		private PlayerHand _player2Hand;
		[SerializeField, Space]
		private PlayerTable _player1Table;
		[SerializeField]
		private PlayerTable _player2Table;
		[SerializeField, Space]
		private PlayerFace _player1Face;
		[SerializeField]
		private PlayerFace _player2Face;

		private Card[] _deck1;
		private Card[] _deck2;
		[SerializeField]
		private float _offsetDeck = 0.5f;
		[SerializeField]
		private Transform _deck1Parent;
		[SerializeField]
		private Transform _deck2Parent;

		[SerializeField, Space]
		private Camera _camera;

		[SerializeField, Space]
		private NextMoveButtonComponent _nextMoveButtonComponent;

		public PlayerNumber _playerMove { get; set; } = PlayerNumber.Player1;

		private void Awake() {
			IEnumerable<CardPropertiesData> arrayCards = new List<CardPropertiesData>();

			foreach(var pack in _packs) arrayCards = pack.UnionProperties(arrayCards);

			_allCards = new List<CardPropertiesData>(arrayCards);

			_baseMat = Shader.Find("TextMeshPro/Sprite");
		}

		private void Start() {
			_deck1 = CreateDeckById(_deck1Parent, PlayerNumber.Player1);
			_deck2 = CreateDeckById(_deck2Parent, PlayerNumber.Player2);

			for(int j = 0; j < 10; j++) {
				var index = -1;

				for(int i = 0; i < _deck1.Length; i++) {
					if(_deck1[i] != null) {
						index = i;
						continue;
					}
				}

				if(index < 0) return;

				_player1Hand.SetNewCard(_deck1[index]);
				_deck1[index] = null;
			}

			for(int j = 0; j < 10; j++) {
				var index = -1;

				for(int i = 0; i < _deck2.Length; i++) {
					if(_deck2[i] != null) {
						index = i;
						continue;
					}
				}

				if(index < 0) return;

				_player2Hand.SetNewCard(_deck2[index]);
				_deck2[index] = null;
			}

			StartCoroutine(MoveCardInTableFromHand());
		}

		private void RandomDeck(Card[] deck) {
			var random = new System.Random(DateTime.Now.Millisecond);
			deck = deck.OrderBy(x => random.Next()).ToArray();
		}

		private Card[] CreateDeckById(Transform parent, PlayerNumber playerNumber) {
			var cards = new Card[_cardDeckCount];
			var vector = new Vector3();
			for(int i = 0; i < _cardDeckCount; i++) {
				cards[i] = Instantiate(_cardPrefab);
				cards[i].transform.position = parent.TransformPoint(vector);
				cards[i].transform.rotation = parent.transform.rotation;
				var card = cards[i].GetComponent<Card>();
				card.PlayerNumber = playerNumber;
				card.PlayerTable = playerNumber == PlayerNumber.Player1 ? _player1Table : _player2Table;


				vector += new Vector3(0f, _offsetDeck, 0f);
				var random = _allCards[UnityEngine.Random.Range(0, _allCards.Count)];

				var newMat = new Material(_baseMat) {
					renderQueue = 2995,
					mainTexture = random.Texture
				};

				cards[i].Configuration(random, newMat, CardUtility.GetDescriptionById(random.Id));
			}

			return cards;
		}

		private Card[] CreateRandomDeck(Transform parent, PlayerNumber playerNumber) {
			var cards = new Card[_cardDeckCount];
			var vector = new Vector3();
			for(int i = 0; i < _cardDeckCount; i++) {
				cards[i] = Instantiate(_cardPrefab);
				cards[i].transform.position = parent.TransformPoint(vector);
				cards[i].transform.rotation = parent.transform.rotation;
				var card = cards[i].GetComponent<Card>();
				card.PlayerNumber = playerNumber;
				card.PlayerTable = playerNumber == PlayerNumber.Player1 ? _player1Table : _player2Table;


				vector += new Vector3(0f, _offsetDeck, 0f);
				var random = _allCards[UnityEngine.Random.Range(0, _allCards.Count)];

				var newMat = new Material(_baseMat) {
					renderQueue = 2995,
					mainTexture = random.Texture
				};

				cards[i].Configuration(random, newMat, CardUtility.GetDescriptionById(random.Id));
			}

			return cards;
		}

		public bool MoveCardInTable(Card card) {
			var table = card.PlayerNumber == PlayerNumber.Player1 ? _player1Table : _player2Table;
			return table.SetNewCard(card, card.PlayerNumber == PlayerNumber.Player1 ? _player1Hand : _player2Hand);
		}

		private IEnumerator MoveCardInTableFromHand() {
			yield return new WaitForSeconds(3f);
			
			for(int j = 0; j < 3; j++) {
				var index = -1;

				for(int i = 0; i < _player2Hand._cardsInHand.Length; i++) {
					if(_player2Hand._cardsInHand[i] != null) {
						index = i;
						continue;
					}
				}

				_player2Hand._cardsInHand[index].transform.rotation = Quaternion.Euler(0f, 0f, 180f);
				MoveCardInTable(_player2Hand._cardsInHand[index]);
			}
		}

		public void NextMove() {
			StartCoroutine(RotateCameraZ180Angles());

			StartCoroutine(RotateObjectY180Angles(_player1Face.transform));
			StartCoroutine(RotateObjectY180Angles(_player2Face.transform));
			StartCoroutine(RotateObjectY180Angles(_nextMoveButtonComponent.transform));

			_player1Table.RotateCards();
			_player2Table.RotateCards();

			_playerMove = _playerMove == PlayerNumber.Player1 ? PlayerNumber.Player2 : PlayerNumber.Player1;
		}

		private IEnumerator RotateCameraZ180Angles() {
			var time = 0f;
			var startRot = _camera.transform.rotation;
			var endRot = Quaternion.Euler(startRot.eulerAngles.x, startRot.eulerAngles.y + (startRot.eulerAngles.y == 0f ? 180f : -180f), startRot.eulerAngles.z);
			while(_camera.transform.rotation.eulerAngles.y != endRot.eulerAngles.y) {
				_camera.transform.rotation = Quaternion.Lerp(startRot, endRot, time * 2f);
				time += Time.deltaTime;

				yield return null;
			}
		}

		public IEnumerator RotateObjectY180Angles(Transform transform) {
			var time = 0f;
			var startRot = transform.rotation;
			var endRot = Quaternion.Euler(startRot.eulerAngles.x, startRot.eulerAngles.y + (startRot.eulerAngles.y == 0f ? 180f : -180f), startRot.eulerAngles.z);
			while(transform.rotation.eulerAngles.y != endRot.eulerAngles.y) {
				transform.rotation = Quaternion.Lerp(startRot, endRot, time * 2f);
				time += Time.deltaTime;

				yield return null;
			}
		}

		public bool CanMoveCardToTable(Card card) {
			PlayerFace player;

			if(card.PlayerNumber == _player1Face.PlayerNumber) {
				player = _player1Face;
			}
			else {
				player = _player2Face;
			}

			if(player.Mana < card.Cost) {
				return false;
			}
			else {
				return true;
			}
		}
	}
}