﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Cards {
	public class Card : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IDragHandler, IEndDragHandler {
		[SerializeField]
		private GameObject _frontCard;
		[SerializeField]
		private MeshRenderer _picture;
		[SerializeField]
		private TextMeshPro _name;
		[SerializeField]
		private TextMeshPro _description;
		[SerializeField]
		private TextMeshPro _cost;
		[SerializeField]
		private TextMeshPro _attack;
		[SerializeField]
		private TextMeshPro _health;
		[SerializeField]
		private TextMeshPro _type;

		public CardsStateType StateType { get; set; } = CardsStateType.InDeck;
		public PlayerNumber PlayerNumber { get; set; }
		public bool IsFrontSide => _frontCard.activeSelf;

		private GameManager _gameManager;

		private Vector3 _lastPosition;

		private void Start() {
			_gameManager = FindObjectOfType<GameManager>();
		}

		public void Configuration(CardPropertiesData data, Material picture, string description) {
			_name.text = data.Name;

			var array = description.Replace("<b>", "").Replace("</b>", "").Replace("\n", "").Trim().Split(' ');
			if(array.Length <= 4) {
				_description.fontSize = 1.5f;
			}
			_description.text = description;
			_cost.text = data.Cost.ToString();
			_attack.text = data.Attack.ToString();
			_health.text = data.Health.ToString();
			_type.text = data.Type == CardUnitType.None ? "" : data.Type.ToString();
			
			_picture.material = picture;
		}

		public void OnBeginDrag(PointerEventData eventData) {
			_lastPosition = transform.position;
		}

		public void OnDrag(PointerEventData eventData) {
			if(_gameManager._playerMove != PlayerNumber) return;

			if(StateType == CardsStateType.InHand || StateType == CardsStateType.OnTable) {
				var pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y));
				transform.position = new Vector3(pos.x, transform.position.y, pos.z);
			}
		}

		public void OnEndDrag(PointerEventData eventData) {
			if(_gameManager._playerMove != PlayerNumber) return;

			if(StateType == CardsStateType.InHand || StateType == CardsStateType.OnTable) {
				var hits = Physics.RaycastAll(transform.position, transform.up, 10000f);
				if(hits.Length > 0) {
					foreach(var hit in hits) {
						var table = hit.transform.GetComponent<PlayerTable>();
						var card = hit.transform.GetComponent<Card>();
						var face = hit.transform.GetComponent<PlayerFace>();

						if(table != null && table.PlayerNumber == PlayerNumber && StateType != CardsStateType.OnTable) {
							_gameManager.MoveCardInTable(this, table);

							break;
						}
						else if(table != null && table.PlayerNumber == PlayerNumber && StateType == CardsStateType.OnTable) {
							StartCoroutine(MoveInLastPosition());

							break;
						}

						if(card != null && card.PlayerNumber != PlayerNumber && card.StateType == CardsStateType.OnTable) {
							

							break;
						}
					}
				}
			}
		}

		public void OnPointerEnter(PointerEventData eventData) {
			if(_gameManager._playerMove != PlayerNumber) return;

			var offsetPos = 30f;
			var offsetScl = 35f;
			if(StateType == CardsStateType.InHand || StateType == CardsStateType.OnTable) {
				transform.position += new Vector3(0f, offsetPos, PlayerNumber == PlayerNumber.Player1 ? offsetPos : -offsetPos) ;
				transform.localScale += new Vector3(offsetScl, 0f, offsetScl);
			}
		}

		public void OnPointerExit(PointerEventData eventData) {
			if(_gameManager._playerMove != PlayerNumber) return;

			var offsetPos = 30f;
			var offsetScl = 35f;
			if(StateType == CardsStateType.InHand || StateType == CardsStateType.OnTable) {
				transform.position -= new Vector3(0f, offsetPos, PlayerNumber == PlayerNumber.Player1 ? offsetPos : -offsetPos);
				transform.localScale -= new Vector3(offsetScl, 0f, offsetScl);
			}
		}

		[ContextMenu("Switch Enable")]
		public void SwitchEnable() {
			var boolean = !IsFrontSide;
			_frontCard.SetActive(boolean);
			_picture.enabled = boolean;
		}

		private IEnumerator MoveInLastPosition() {
			var time = 0f;
			var startPos = transform.position;
			var endPos = new Vector3(_lastPosition.x, _lastPosition.y - 30f, _lastPosition.z - 30f);
			while(transform.position != endPos) {
				transform.position = Vector3.Lerp(startPos, endPos, time * 2f);
				time += Time.deltaTime;
				yield return null;
			}
		}
	}
}