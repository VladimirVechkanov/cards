﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards {
	public class PlayerHand : MonoBehaviour {
		[SerializeField]
		private Transform[] _positions;
		private Card[] _cardsInHand;

		private void Start() {
			_cardsInHand = new Card[_positions.Length];
		}

		private int GetLastPosition() {
			for(int i = 0; i < _cardsInHand.Length; i++) {
				if(_cardsInHand[i] == null) return i;
			}

			return -1;
		}

		public bool SetNewCard(Card newCard) {
			var res = GetLastPosition();

			if(res < 0) {
				Destroy(newCard.gameObject);
				return false;
			}

			_cardsInHand[res] = newCard;
			StartCoroutine(MoveInHand(newCard, _positions[res]));

			return true;
		}

		private IEnumerator MoveInHand(Card newCard, Transform place) {
			var time = 0f;
			var startPos = newCard.transform.position;
			var endPos = new Vector3(startPos.x, startPos.y + 50f, startPos.z);

			while(time < 0.5f) {
				newCard.transform.position = Vector3.Lerp(startPos, endPos, time);
				time += Time.deltaTime;
				yield return null;
			}

			time = 0f;
			var startRot = newCard.transform.rotation;
			var endRot = Quaternion.Euler(0f, 0f, 360f);
			while(newCard.transform.rotation.eulerAngles.z < 180f) {
				newCard.transform.rotation = Quaternion.Lerp(startRot, endRot, time);
				time += Time.deltaTime;
				yield return null;
			}
			newCard.SwitchEnable();

			//time = 0f;
			//startPos = newCard.transform.position;
			//endPos = place.position;
			//while(time < 1f) {
			//	newCard.transform.position = Vector3.Lerp(startPos, endPos, time);
			//	time += Time.deltaTime;
			//	yield return null;
			//}

			newCard.StateType = CardsStateType.InHand;
		}
	}
}