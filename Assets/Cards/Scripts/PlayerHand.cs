﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards {
	public class PlayerHand : MonoBehaviour {
		[SerializeField]
		private Transform[] _positions;
		public Card[] _cardsInHand;

		public PlayerNumber PlayerNumber { get; set; }

		private void Start() {
			PlayerNumber = name == "Player1Hand" ? PlayerNumber.Player1 : PlayerNumber.Player2;
			_cardsInHand = new Card[_positions.Length];
		}

		private void Update() {
			
		}

		private int GetLastPosition() {
			for(int i = 0; i < _cardsInHand.Length; i++) {
				if(_cardsInHand[i] == null) return i;
			}

			return -1;
		}

		public bool SetNewCard(Card newCard) {
			var res = GetLastPosition();

			if(res < 0) {
				Destroy(newCard.gameObject);
				return false;
			}

			_cardsInHand[res] = newCard;
			StartCoroutine(MoveInHand(newCard, _positions[res]));

			return true;
		}

		private IEnumerator MoveInHand(Card newCard, Transform place) {
			var time = 0f;
			var startPos = newCard.transform.position;
			var endPos = new Vector3(startPos.x, startPos.y + 30f, startPos.z);

			while(newCard.transform.position != endPos) {
				newCard.transform.position = Vector3.Lerp(startPos, endPos, time * 3f);
				time += Time.deltaTime;
				yield return null;
			}

			time = 0f;
			var startRot = newCard.transform.rotation;
			var endRot = Quaternion.Euler(newCard.transform.rotation.eulerAngles.x, newCard.transform.rotation.eulerAngles.y, 180f);
			while(newCard.transform.rotation.eulerAngles.z != endRot.eulerAngles.z) {
				newCard.transform.rotation = Quaternion.Lerp(startRot, endRot, time * 2f);
				time += Time.deltaTime;

				if(!newCard.IsFrontSide && newCard.transform.rotation.eulerAngles.z != 0f && newCard.transform.rotation.eulerAngles.z <= 320f) {
					newCard.SwitchEnable();
				}

				yield return null;
			}

			time = 0f;
			startPos = newCard.transform.position;
			endPos = place.position;
			while(newCard.transform.position != endPos) {
				newCard.transform.position = Vector3.Lerp(startPos, endPos, time);
				time += Time.deltaTime;
				yield return null;
			}

			newCard.StateType = CardsStateType.InHand;
		}

		public void RemoveCard(Card card) {
			for(int i = 0; i < _cardsInHand.Length; i++) {
				if(_cardsInHand[i] == card) {
					_cardsInHand[i] = null;
					break;
				}
			}
		}
	}
}