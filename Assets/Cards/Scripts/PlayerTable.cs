﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards {
	public class PlayerTable : MonoBehaviour {
		public PlayerNumber PlayerNumber { get; set; }

		[SerializeField]
		private Transform[] _positions;
		[SerializeField]
		private Card[] _cardsOnTable;

		[SerializeField]
		private PlayerFace _playerFace;

		private void Start() {
			PlayerNumber = name == "Player1Table" ? PlayerNumber.Player1 : PlayerNumber.Player2;
			_cardsOnTable = new Card[_positions.Length];
		}

		private int GetLastPosition() {
			for(int i = 0; i < _cardsOnTable.Length; i++) {
				if(_cardsOnTable[i] == null) return i;
			}

			return -1;
		}

		public bool SetNewCard(Card newCard, PlayerHand playerHand) {
			var res = GetLastPosition();

			if(res < 0) {
				return false;
			}

			_cardsOnTable[res] = newCard;
			_playerFace.SetMana(newCard.Cost);
			playerHand.RemoveCard(newCard);
			StartCoroutine(MoveInTable(newCard, _positions[res]));

			return true;
		}

		private IEnumerator MoveInTable(Card newCard, Transform place) {
			var time = 0f;
			var startPos = newCard.transform.position;
			var endPos = new Vector3(startPos.x, startPos.y + 30f, startPos.z);

			time = 0f;
			startPos = newCard.transform.position;
			endPos = place.position;
			while(newCard.transform.position != endPos) {
				newCard.transform.position = Vector3.Lerp(startPos, endPos, time * 2f);
				time += Time.deltaTime;
				yield return null;
			}

			newCard.StateType = CardsStateType.OnTable;
		}

		public void RemoveDestroyCards() {
			for(int i = 0; i < _cardsOnTable.Length; i++) {
				if(_cardsOnTable[i] != null && _cardsOnTable[i].StateType == CardsStateType.Discard) {
					_cardsOnTable[i] = null;
					break;
				}
			}
		}

		public void RotateCards() {
			foreach(var card in _cardsOnTable) {
				if(card != null) {
					StartCoroutine(RotateCard(card));
				}
			}
		}

		private IEnumerator RotateCard(Card card) {
			var time = 0f;
			var startRot = card.transform.rotation;
			var endRot = Quaternion.Euler(startRot.eulerAngles.x, startRot.eulerAngles.y + (startRot.eulerAngles.y == 0f ? 180f : -180f), startRot.eulerAngles.z);
			while(card.transform.rotation.eulerAngles.y != endRot.eulerAngles.y) {
				card.transform.rotation = Quaternion.Lerp(startRot, endRot, time * 2f);
				time += Time.deltaTime;

				yield return null;
			}
		}
	}
}