﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards {
	public class PlaceInHand : MonoBehaviour {
		public PlayerNumber PlayerNumber { get; set; }

		private void Start() {
			PlayerNumber = transform.parent.name == "Player1Hand" ? PlayerNumber.Player1 : PlayerNumber.Player2;
		}
	}
}