﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Cards {
	public class PlayerFace : MonoBehaviour {
		[SerializeField]
		private TextMeshPro _health;
		[SerializeField]
		private TextMeshPro _mana;

		[NonSerialized]
		public int Health;
		[NonSerialized]
		public int Mana;

		public string[] CardsId;

		public PlayerNumber PlayerNumber { get; set; }
		public SideType SideType { get; set; }

		private void Start() {
			PlayerNumber = name == "Player1Face" ? PlayerNumber.Player1 : PlayerNumber.Player2;

			_mana.text = "10";

			Health = Int32.Parse(_health.text);
			Mana = Int32.Parse(_mana.text);
		}

		public void TakeDamage(int damage) {
			Health -= damage;
			_health.text = Health.ToString();

			if(Health <= 0) {
				Debug.Log(PlayerNumber == PlayerNumber.Player1 ? PlayerNumber.Player2.ToString() : PlayerNumber.Player1.ToString() + " Win");

				Time.timeScale = 0f;
			}
		}

		public void SetMana(int mana) {
			Mana -= mana;
			_mana.text = Mana.ToString();
		}
	}
}